<?php

class ImageStyleAllRootFilesStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return 'sites/all';
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . drupal_encode_path($path);
  }

}
