<?php

/**
 * Implements hook_image_style_path_alter().
 */
function image_style_all_image_style_path_alter(&$path, $style, $image_uri) {
  $scheme = file_uri_scheme($image_uri);
  if ($scheme == 'site') {
    $path = str_replace('site://', 'public://', $path);
  }
  elseif ($scheme == 'all') {
    $path = str_replace('all://', 'public://', $path);
  }
}
