Use any path inside Drupal root to image style images. For example:

* sites/default/files/styles/thumbnail/<strong>site</strong>/images/bench.jpg
  for file sites/default/images/bench.jpg
* sites/default/files/styles/thumbnail/<strong>all</strong>/themes/mytheme/logo.png
  for file sites/all/themes/mytheme/logo.png

You can do this, because of 2 new file schemes/stream wrappers: `site://` (looks inside `sites/CURRENT_SITE`)
and `all://` (looks inside `sites/all`).
