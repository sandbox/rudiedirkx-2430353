<?php

class ImageStyleAllSiteFilesStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return conf_path();
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . $this->getDirectoryPath() . '/' . drupal_encode_path($path);
  }

}
