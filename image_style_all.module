<?php

/**
 * Implements hook_menu_alter().
 */
function image_style_all_menu_alter(&$items) {
  $directory_path = file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();
  $items[$directory_path . '/styles/%image_style']['page callback'] = 'image_style_all_image_style_deliver';
}

/**
 * Page callback for sites/SITE/files/styles/%image_style/SCHEME/FILE.
 *
 * This is a literal copy of Drupal 7.34's image_style_deliver(), except I added
 * a `drupal_alter('image_style_path')`.
 */
function image_style_all_image_style_deliver($style, $scheme) {
  $args = func_get_args();
  array_shift($args);
  array_shift($args);
  $target = implode('/', $args);

  // Check that the style is defined, the scheme is valid, and the image
  // derivative token is valid. (Sites which require image derivatives to be
  // generated without a token can set the 'image_allow_insecure_derivatives'
  // variable to TRUE to bypass the latter check, but this will increase the
  // site's vulnerability to denial-of-service attacks. To prevent this
  // variable from leaving the site vulnerable to the most serious attacks, a
  // token is always required when a derivative of a derivative is requested.)
  $valid = !empty($style) && file_stream_wrapper_valid_scheme($scheme);
  if (!variable_get('image_allow_insecure_derivatives', FALSE) || strpos(ltrim($target, '\/'), 'styles/') === 0) {
    $valid = $valid && isset($_GET[IMAGE_DERIVATIVE_TOKEN]) && $_GET[IMAGE_DERIVATIVE_TOKEN] === image_style_path_token($style['name'], $scheme . '://' . $target);
  }
  if (!$valid) {
    return MENU_ACCESS_DENIED;
  }

  $image_uri = $scheme . '://' . $target;
  $derivative_uri = image_style_path($style['name'], $image_uri);
  drupal_alter('image_style_path', $derivative_uri, $style, $image_uri);

  // If using the private scheme, let other modules provide headers and
  // control access to the file.
  if ($scheme == 'private') {
    if (file_exists($derivative_uri)) {
      file_download($scheme, file_uri_target($derivative_uri));
    }
    else {
      $headers = file_download_headers($image_uri);
      if (in_array(-1, $headers) || empty($headers)) {
        return MENU_ACCESS_DENIED;
      }
      if (count($headers)) {
        foreach ($headers as $name => $value) {
          drupal_add_http_header($name, $value);
        }
      }
    }
  }

  // Confirm that the original source image exists before trying to process it.
  if (!is_file($image_uri)) {
    watchdog('image', 'Source image at %source_image_path not found while trying to generate derivative image at %derivative_path.',  array('%source_image_path' => $image_uri, '%derivative_path' => $derivative_uri));
    return MENU_NOT_FOUND;
  }

  // Don't start generating the image if the derivative already exists or if
  // generation is in progress in another thread.
  $lock_name = 'image_style_deliver:' . $style['name'] . ':' . drupal_hash_base64($image_uri);
  if (!file_exists($derivative_uri)) {
    $lock_acquired = lock_acquire($lock_name);
    if (!$lock_acquired) {
      // Tell client to retry again in 3 seconds. Currently no browsers are known
      // to support Retry-After.
      drupal_add_http_header('Status', '503 Service Unavailable');
      drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
      drupal_add_http_header('Retry-After', 3);
      print t('Image generation in progress. Try again shortly.');
      drupal_exit();
    }
  }

  // Try to generate the image, unless another thread just did it while we were
  // acquiring the lock.
  $success = file_exists($derivative_uri) || image_style_create_derivative($style, $image_uri, $derivative_uri);

  if (!empty($lock_acquired)) {
    lock_release($lock_name);
  }

  if ($success) {
    $image = image_load($derivative_uri);
    file_transfer($image->source, array('Content-Type' => $image->info['mime_type'], 'Content-Length' => $image->info['file_size']));
  }
  else {
    watchdog('image', 'Unable to generate the derived image located at %path.', array('%path' => $derivative_uri));
    drupal_add_http_header('Status', '500 Internal Server Error');
    drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
    print t('Error generating image.');
    drupal_exit();
  }
}

/**
 * Implements hook_stream_wrappers().
 */
function image_style_all_stream_wrappers() {
  return array(
    'site' => array(
      'name' => t('Non-uploads, this site'),
      'class' => 'ImageStyleAllSiteFilesStreamWrapper',
      'description' => t('Files not uploaded by users, but saved in **site-specific** modules and themes.'),
      'type' => STREAM_WRAPPERS_LOCAL_NORMAL,
    ),
    'all' => array(
      'name' => t('Non-uploads, Drupal root'),
      'class' => 'ImageStyleAllRootFilesStreamWrapper',
      'description' => t('Files not uploaded by users, but saved anywhere in sites/all.'),
      'type' => STREAM_WRAPPERS_LOCAL_NORMAL,
    ),
  );
}

/**
 * Implements hook_image_style_path_alter().
 */
function image_style_all_image_style_path_alter(&$path, $style, $image_uri) {
  $scheme = file_uri_scheme($image_uri);

  $recognized = array('site', 'root');
  if (module_exists('system_stream_wrapper')) {
    $recognized = array_merge($recognized, array_keys(system_stream_wrapper_stream_wrappers()));
  }

  if (in_array($scheme, $recognized)) {
    $path = str_replace($scheme . '://', 'public://', $path);
  }
}
